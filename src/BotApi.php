<?php
/**
 * Author: Vadim
 * Date: 2019-08-30
 */
declare(strict_types=1);

namespace Tele;

use TelegramBot\Api\Exception;
use TelegramBot\Api\HttpException;
use TelegramBot\Api\InvalidJsonException;

class BotApi extends \TelegramBot\Api\BotApi
{
    /**
     * @var string
     */
    protected $proxy;

    /**
     * Call method
     *
     * @param string $method
     * @param array|null $data
     *
     * @return mixed
     * @throws Exception
     * @throws HttpException
     * @throws InvalidJsonException
     */
    public function call($method, array $data = null)
    {
        $options = [
            CURLOPT_URL            => $this->getUrl() . '/' . $method,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST           => null,
            CURLOPT_POSTFIELDS     => null,
            CURLOPT_PROXY          => $this->proxy,
        ];

        if ($data) {
            $options[CURLOPT_POST] = true;
            $options[CURLOPT_POSTFIELDS] = $data;
        }

        $response = self::jsonValidate($this->executeCurl($options), $this->returnArray);

        if ($this->returnArray) {
            if (!isset($response['ok'])) {
                throw new Exception($response['description'], $response['error_code']);
            }

            return $response['result'];
        }

        if (!$response->ok) {
            throw new Exception($response->description, $response->error_code);
        }

        return $response->result;
    }

    /**
     * @param string $proxy
     */
    public function setProxy(string $proxy)
    {
        $this->proxy = $proxy;
    }
}