<?php
/**
 * Author: Vadim
 * Date: 2019-08-13
 */
declare(strict_types=1);

namespace Tele;

use TelegramBot\Api\Events\EventCollection;
use Tele\Types\Update;

/**
 * Class Client
 *
 * @package Insta
 */
class Client extends \TelegramBot\Api\Client
{
    /**
     * Client constructor.
     *
     * @param $token
     * @param null $trackerToken
     */
    public function __construct($token, $trackerToken = null)
    {
        $this->api = new BotApi($token);
        $this->events = new EventCollection($trackerToken);
    }

    /**
     * @return BotApi
     */
    public function getApi(): BotApi
    {
        return $this->api;
    }

    /**
     * Webhook handler
     *
     * @return array
     * @throws \TelegramBot\Api\InvalidJsonException
     */
    public function run()
    {
        if ($data = \TelegramBot\Api\BotApi::jsonValidate($this->getRawBody(), true)) {
            $this->handle([Update::fromResponse($data)]);
        }
    }
}