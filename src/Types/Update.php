<?php
/**
 * Author: Vadim
 * Date: 2019-08-14
 */
declare(strict_types=1);

namespace Tele\Types;

use TelegramBot\Api\Types\CallbackQuery;
use TelegramBot\Api\Types\Inline\ChosenInlineResult;
use TelegramBot\Api\Types\Inline\InlineQuery;
use TelegramBot\Api\Types\Message;

/**
 * Class Update
 *
 * @package Insta\Types
 */
class Update extends \TelegramBot\Api\Types\Update
{
    /**
     * @var Message
     */
    protected $channelPost;

    /**
     * @var array
     */
    protected static $map = [
        'update_id'            => true,
        'message'              => Message::class,
        'inline_query'         => InlineQuery::class,
        'chosen_inline_result' => ChosenInlineResult::class,
        'callback_query'       => CallbackQuery::class,
        'channel_post'         => Message::class,
    ];

    /**
     * @param Message $message
     */
    public function setChannelPost(Message $message)
    {
        $this->channelPost = $message;
    }

    /**
     * @return Message
     */
    public function getChannelPost()
    {
        return $this->channelPost;
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return parent::getMessage() ?: $this->getChannelPost();
    }
}